#ifndef __COMMAND_H__
#define __COMMAND_H__
#include <string>
#include <iostream>
#include "board.h"

class CmdNode {
	enum {alphabet=52};
	
	private:
	std::string command;
	CmdNode *letters[alphabet];
	int toSpot(char letter); //convert from char to int
	char fromSpot(int spot); //convert from int to char

	public:
	CmdNode(); //constructor
	~CmdNode(); //destructor
	void init(); //initialize the root CmdNode
	void insert(const std::string &keyword, const std::string &command); //adds a command to a keyword (at the appropriate node)
	void remove(const std::string &keyword); //removes a command from a keyword
	int findAmount(); //finds the amount of commands within the current node and its subnodes
	std::string findCommand(const std::string &keyword); //finds the only command within the current node and its subnodes
	void rename(const std::string &current, const std::string &newcmd); //renames a command name to newcmd
	void interpretCommand(Board *board); //interprets commands from stdin
};

#endif
