#ifndef __BOARD_H__
#define __BOARD_H__
#include <iostream>
#include <sstream>
#include "window.h"
#include "tetro.h"

class Board {
	enum {rows=18, cols=10, Tetros=7, blocksPerTetro=4, maxLevel=3, maxTetros=250, gameOverMessage=8};
	
	private:
	Xwindow *mainwin;
	Xwindow *secwin;
	bool graphics;
	int bseed;
	bool gameOver;
	int score;
	int highscore;
	int level;
	int TetroProbs[Tetros];
	int ProbNext;
	IndBlock *boardBlocks[cols][rows];
	Tetro *currentTetro;
	char nextTetro;
	int nextTetroi;
	char listTetro[maxTetros];
	
	int scoreRow(); //adds score for removal of complete row
	
	public:
	Board(); //constructor
	~Board(); //destructor
	int stringToInt(std::string s); //returns an int that represents the given string
	std::string intToString(int n); //returns a string that represents the given integer
	bool getGameOver(); //returns a bool representing whether the game is over or not
	Tetro *getCurrentTetro(); //returns a pointer to the current Tetro
	IndBlock *getIndBlock(int x, int y); //returns a pointer to the IndBlock on (x,y) on the board
	void moveIndBlock(int x1, int y1, int x2, int y2); //moves the IndBlock from (x1, y1) to (x2, y2) on the board
	bool isFree(int x, int y); //checks if the space on the board is free of IndBlocks
	void checkRows(int y[blocksPerTetro]); //checks array of integers as 4 y-values and calls isFree
	void scoreTetro(Tetro *t); //adds score for removal of complete Tetro
	void scoreRow(int rows); //adds score for clearing int rows rows
	void createTetro(); //creates new Tetro object (randomly selects type and orientation)
	void generateNextTetro(); //generates the next Tetro
	void interpretCommandLine(int argc, char *argv[]); //interprets information from the command line
	void getProbs(); //reads a text file leveli.txt where i is level and assigns appropriate probabilities to all of TetroProbs
	void endGame(); //ends the game
	void restart(); //restart game
	void levelUp(); //increases the game’s level
	void levelDown(); //decreases the game’s level
	void checkIndBlocks(Tetro *t); //checks all IndBlocks in Tetro t and calls Tetro's destructor if all are NULL
	void shiftBoardBlocks(int); //move the all inBlocks in boardBlocks above int down one
	void drawBlock(int x, int y, char ind, Xwindow *win); //draws individual block to Xwindow win at x and y
	
	friend std::ostream& ::operator<<(std::ostream &out, Board &board); //prints board to stdout
};

#endif
