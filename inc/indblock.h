#ifndef __INDBLOCK_H__
#define __INDBLOCK_H__
#include <iostream>
#include "board.h"

class Board;

class Tetro;

class IndBlock {
	enum {blocksPerTetro=4};
	
	private:
	Tetro *myTetro;
	char blockType; //the type of block (one of L, J, S, Z, O, T, I)
	int x;
	int y;

	public:
	IndBlock(); //constructor
	~IndBlock(); //destructor sets Tetro’s pointer to this IndBlock to NULL and calls its checkIndBlocks(); function
	Tetro *getMyTetro(); //returns IndBlock's Tetro
	char getBlockType(); //returns blockType
	int getX(); //returns x
	int getY(); //returns y
	void setMyTetro(Tetro *); //sets IndBlock's Tetro
	void setBlockType(char); //sets blockType
	void setCoords(int, int); //sets X and Y coordinates
	
	friend std::ostream& ::operator<<(std::ostream &out, IndBlock &block); //prints IndBlock’s blockType to stdout
};

#endif
