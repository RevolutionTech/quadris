#ifndef __TETRO_H__
#define __TETRO_H__
#include "indblock.h"

class Tetro {
	enum {blocksPerTetro=4};
	
	private:
	Board *board; //the board
	IndBlock *tetroBlocks[blocksPerTetro];
	char blockType; //the type of block (one of L, J, S, Z, O, T, I)
	int x; //(of bottom-left individual block)
	int y; //(of bottom-left individual block)
	int blockLevel; //level when Tetro was created
	int orientation; //contains the current rotation phase of the Tetro;
	bool iRotate(std::string); //rotates an i block
	bool sRotate(std::string); //rotates an s block
	bool zRotate(std::string); //rotates an z block
	bool jRotate(std::string); //rotates an j block
	bool lRotate(std::string); //rotates an l block
	bool tRotate(std::string); //rotates an t block

	public:
	Tetro(); //constructor
	Board *getBoard(); //returns Tetro's Board
	IndBlock *getTetroBlock(int); //returns int ith IndBlock from the Tetro
	char getBlockType(); //returns blockType
	int getX(); //returns x
	int getY(); //returns y
	int getLargest(int num[4]); //returns max out of array
	int getSmallest(int num[4]); //returns min out of array
	int getBlockLevel(); //returns blockLevel
	void setBoard(Board *); //sets the pointer to the Board
	void setTetroBlock(int, IndBlock *); //sets int ith IndBlock from the Tetro
	void setBlockType(char); //sets the blockType
	void setX(int); //sets X
	void setY(int); //sets Y
	void setBlockLevel(int); //sets the block level
	bool rClockwise(); //rotates the block clockwise
	bool rCClockwise(); //rotates the block counter-clockwise
	void drop(); //drops the block until collision
	void lShift(); //moves the block to the left one unit
	void rShift(); //moves the block to the right one unit
	void dShift(); //moves the block down one unit
	bool inTetro(int, int); //check if the given x,y is part of the tetro
	void getOrder(int *, std::string); //get the order for movement
	int getOrientation();
	void setOrientation(int);
};

#endif
