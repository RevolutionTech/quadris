OBJDIR=out
SRCDIR=src
LIBDIR=lib
BINDIR=bin
TSTDIR=test
INCDIR=inc
DEPDIR=deps
INCLUDE=-I${INCDIR}
CXX=g++
CXXFLAGS=-I${INCDIR} -g -Wall -c
SOURCES=${wildcard ${SRCDIR}/*.cc}
OBJECTS=${patsubst ${SRCDIR}/%.cc, ${OBJDIR}/%.o,${SOURCES}}
EXEC=quadris
cmdTestBuild=${CXX} -g -Wall ${INCLUDE} $^ $(TSTDIR)/$@.cc -o $(BINDIR)/$@

vpath %.test.cc ${TSTDIR}
vpath %.test.o ${BINDIR}
vpath %.cc ${SRCDIR}
vpath %.o ${OBJDIR}
vpath %.h ${INCDIR}

$(EXEC) : $(OBJECTS) main.cc
	$(CXX) -g -Wall $(INCLUDE) -L/usr/lib/x86_64-linux-gnu $(OBJDIR)/*.o main.cc -lX11 -o $(EXEC)

${OBJDIR}/%.o : $(addprefix $(SRCDIR)/, %.cc)
	$(CXX) ${CXXFLAGS} $< -o $@

.PHONY : clean
clean:
	rm -f $(OBJDIR)/*.o  $(BINDIR)/* $(EXEC) > /dev/null  2>&1

