#include <iostream>
#include "board.h"
#include "command.h"

using namespace std;

int main(int argc, char *argv[]){
	Board *b = new Board;
	b->interpretCommandLine(argc, argv);
	std::cout << *b << std::endl;
	
	//interpreter
	CmdNode *c = new CmdNode;
	c->init();
	while(!std::cin.eof()){
		c->interpretCommand(b);
	}
	delete c;
	
	delete b;
	
	return 0;
}
