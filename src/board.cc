#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <math.h> //for pow
#include <time.h> //for time
#include "PRNG.h"
#include "board.h"

Board::Board() : graphics(true), bseed(0), gameOver(false), score(0), highscore(0), level(0), ProbNext(0), nextTetroi(0) {
	TetroProbs[0] = 1;
	for (int i=1; i<Tetros; ++i){
		TetroProbs[i] = TetroProbs[i-1] + 1;
	}
	getProbs();
} //constructor

Board::~Board(){
	restart();
	if (graphics){
		delete secwin;
		delete mainwin;
	}
} //destructor

//Purpose:	consumes a string and produces an int; interprets the int from
//			a stringstream of the given string
//Pre:		True
//Post:		an int representing the given string is returned
int Board::stringToInt(std::string s){
	std::istringstream ss(s);
    int n;
    ss >> n;
	return n;
}

//Purpose:	consumes a int and produces a string; consumes an integer and
//			adds it to a stringstream, then returns a completed string
//Pre:		True
//Post:		a string representing the given integer is returned
std::string Board::intToString(int i){
	std::ostringstream sout;
	sout << i;
	return sout.str();
}

//Purpose:	consumes an int and an array of char pointers and produces void; updates the
//			Board to reflect information from the command line and restarts the Board
//Pre:		True
//Post:		Produces void; updates the Board to reflect information from the command line
//			and then restarts the Board
void Board::interpretCommandLine(int argc, char *argv[]){
	for (int i=0; i<argc; ++i){
		std::string args = argv[i];
		if (args == "-text"){
			graphics = false;
		}
		else if (args == "-seed" && i<argc-1){
			std::string seednum = argv[i+1];
			bseed = stringToInt(seednum);
		}
	}
	if (graphics){
		mainwin = new Xwindow(320,569);
		secwin = new Xwindow(200,164);
	}
	restart();
}

//Purpose:	consumes void and produces void; sets all of the Tetro's probabilities
//			of appearing or imports the sequence of Tetros from sequence.txt
//Pre:		True
//Post:		produces void; sets all of the Tetro's probabilities of appearing
//			or imports the sequence of Tetros from sequence.txt if level = 0
void Board::getProbs(){
	if (level == 0){
		char nextBlock;
		std::ifstream seqfile ("sequence.txt");
		if (seqfile.is_open()) {
			seqfile >> nextBlock;
			if (seqfile.good()) {
				int i = 0;
				while (!seqfile.eof() && i<maxTetros){
					switch (nextBlock){
						case 'I': case 'O': case 'T': case 'J': case 'L': case 'S': case 'Z': listTetro[i] = nextBlock;
					}
					seqfile >> nextBlock;
					++i;
				}
			}
			seqfile.close();
		}
		else {
			levelUp();
		}
	}
	else {
		char myProb;
		std::string levelfile = "level" + intToString(level) + ".txt";
		std::ifstream probfile (levelfile.c_str());
		if (probfile.is_open()) {
			int i = 0;
			probfile >> myProb;
			while (probfile.good()) {
				if (myProb != ' '){
					if (i == 0){
						TetroProbs[0] = myProb - '0';
					}
					else if (i<Tetros){
						TetroProbs[i] = TetroProbs[i-1] + myProb - '0';
					}
				}
				++i;
				probfile >> myProb;
			}
			probfile.close();
		}
	}
}

//Purpose:	consumes void and produces void; restarts the Board
//Pre:		True
//Post:		produces void; restarts the Board
void Board::restart(){
	gameOver = true;
	score = 0;
	nextTetroi = 0;
	for (int i=0; i<rows; ++i){
		for (int j=0; j<cols; ++j){
			if (boardBlocks[j][i] != NULL){
				delete boardBlocks[j][i];
				boardBlocks[j][i] = NULL;
			}
		}
	}
	generateNextTetro();
	createTetro();
	gameOver = false;
}

//Purpose:	consumes void and produces a bool that represents whether the game
//			is over or not
//Pre:		True
//Post:		Produces a bool that represents whether the game is over or not
bool Board::getGameOver(){
	return gameOver;
}

//Purpose:	consumes void and produces a pointer to the board's current Tetro
//Pre:		True
//Post:		produces a pointer to the board's current Tetro
Tetro *Board::getCurrentTetro(){
	return currentTetro;
}

//Purpose:	Consumes two ints x and y and produces a pointer to the IndBlock 
//          at position x,y
//Pre:		True
//Post:		produces a pointer to the IndBlock at position x,y
IndBlock *Board::getIndBlock(int x, int y){ 
	return boardBlocks[x][y]; 
}

//Purpose:	Consumes Four ints x1, y1, x2, y2 and moves the indBlock at 
//			position x1, y1 to x2, y2
//          at position x,y
//Pre:		boardBlocks[x2][y2] == NULL
//Post:		moves the indBlock at position x1, y1 to x2, y2
void Board::moveIndBlock(int x1, int y1, int x2, int y2){
	boardBlocks[x2][y2] = boardBlocks[x1][y1];
	boardBlocks[x1][y1] = NULL;
	boardBlocks[x2][y2]->setCoords(x2,y2);
	
}

//Purpose:	Consumes Two ints x and y and produces true if 
//          at position x,y is NULL and false otherwise
//Pre:		True
//Post:		produces true if at position x,y is NULL and false otherwise
bool Board::isFree(int x, int y){  
	if(x >= 0 && x <= 9 && y >= 0 && y <= 17) {
		if(boardBlocks[x][y] == NULL) {
			return true;
		}
	}
	return false;
}

//Purpose:	Consumes an int representing the amount of rows and produces void;
//			adds to the board's score according to the amount of rows removed
//			and the level of the board
//Pre:		True
//Post:		produces void; adds to the board's score in accordance with the
//			amount of rows removed and the level of the board
void Board::scoreRow(int rows){
	if (rows > 0){
		score += pow(rows + level, 2);
	}
	if (highscore <= score) {
		highscore = score;
	}
}

//Purpose:	Consumes a tetro pointer t and produces void; changes the score
//			and highscore to reflect the destruction of the tetro t.
//Pre:		all of t->tetroBlocks[] == NULL
//Post:		produces void; changes the score and highscore to reflect the
//			destruction of the tetro t and deletes t
void Board::scoreTetro(Tetro *t){ 
	int blockLevel = t->getBlockLevel();
	score += pow(blockLevel + 1, 2);
	if (highscore <= score) {
		highscore = score;
	}
}


//Purpose:	Consumes an int[4] y and check every element of y to see if row[y[i]] 
//			is full, if it is it then clears the row and drops the other blocks to
//			match it.  After all given rows are checked it calls scoreRow
//Pre:		True	
//Post:		if row[y[i]] is full, if it is it then clears the row and drops the 
//			other blocks to match it.  After all given rows are checked it calls 
//			scoreRow
void Board::checkRows(int y[4]){
	int checkedRows[4];
	int sortedRows[4];
	int *temp = new int[4];
	int numRowsCleared = 0;
	int val;
	
	for(int i = 0; i < 4; i++) {
		checkedRows[i] = -1;
		temp[i] = y[i];
	}
	
	for(int i = 0; i < 4; i++) {
		val = currentTetro->getSmallest(temp);
		temp[val] = 20;
		sortedRows[i] = y[val];
	}

	bool rowFull = true;
	bool alreadyChecked = false;
	
	for(int i = 0; i < 4; i++) {
		//check if the current row was already checked
		for(int j = 0; j < 4; j++) {
			if(checkedRows[j] == sortedRows[i]) {
				alreadyChecked = true;
			}
		}
		
		if(!alreadyChecked) {
			//check is the row is full
			for(int j = 0; j < 10; j++) {
				if(isFree(j,sortedRows[i])) {
					rowFull = false;
					break;
				}
			}
			
			if(rowFull) {
				checkedRows[i] = sortedRows[i];
				//add 1 to the number of rows cleared
				++numRowsCleared;
				//clear the completed row
				for(int j = 0; j < 10; j++) {
					delete boardBlocks[j][sortedRows[i]];
					boardBlocks[j][sortedRows[i]] = NULL;
				}
				shiftBoardBlocks(sortedRows[i]);
			}
			rowFull = true;
		}
		alreadyChecked = false;
	}
	scoreRow(numRowsCleared);
}

//Purpose: Consumes one int row and shifts all indBlocks above row down one
//Pre: isFree is true for all of row
//Post: all indBlocks above row down are shifted down 1
void Board::shiftBoardBlocks(int row) {
	int currentRow = row-1;
	while(currentRow > 0) {
		for(int i = 0; i < 10; i++) {
			if(!isFree(i,currentRow)) {
				boardBlocks[i][currentRow+1] = boardBlocks[i][currentRow];
				boardBlocks[i][currentRow] = NULL;
				boardBlocks[i][currentRow+1]->setCoords(i, currentRow+1);
			}
		}
		--currentRow;
	}
}

//Purpose:	consumes void and produces void; increases the Board's level by one
//Pre:		True
//Post:		produces void; increases the Board's level by one
void Board::levelUp(){
	if (level < maxLevel){
		++level;
	}
	getProbs();
}

//Purpose:	consumes void and produces void; decreases the Board's level by one
//Pre:		True
//Post:		produces void; decreases the Board's level by one
void Board::levelDown(){
	if (level > 0){
		--level;
	}
	getProbs();
}

//Purpose:	consumes void and produces void; creates a Tetro based on the nextTetro
//			char and sets that Tetro as currentTetro, then generates the nextTetro
//Pre:		nextTetro != ' '
//Post:		produces void; creates a new Tetro based on the next Tetro and then
//			generates a new Tetro
void Board::createTetro(){
	//move next Tetro to current Tetro
	Tetro *t = new Tetro;
	t->setBoard(this);
	t->setBlockType(nextTetro);
	t->setBlockLevel(level);
	IndBlock *blks[blocksPerTetro];
	for (int i=0; i<blocksPerTetro; ++i){
		blks[i] = new IndBlock;
		blks[i]->setMyTetro(t);
	}
	//set random number generator and seed
	PRNG rng;
	if (bseed == 0){
		rng.seed(time(NULL)+1);
	}
	else {
		rng.seed(bseed);
	}
	int myRand = rng(3);
	switch (nextTetro){
		case 'I':
			switch (myRand){
				case 0: case 1:
					blks[0]->setCoords(0,3);
					blks[1]->setCoords(0,4);
					blks[2]->setCoords(0,5);
					blks[3]->setCoords(0,6);
					t->setY(6);
					t->setOrientation(1);
					break;
				case 2: case 3:
					blks[0]->setCoords(0,3);
					blks[1]->setCoords(1,3);
					blks[2]->setCoords(2,3);
					blks[3]->setCoords(3,3);
					t->setY(3);
					t->setOrientation(0);
					break;
			}
			break;
		case 'O':
			blks[0]->setCoords(0,3);
			blks[1]->setCoords(1,3);
			blks[2]->setCoords(0,4);
			blks[3]->setCoords(1,4);
			t->setY(4);
			t->setOrientation(0);
			break;
		case 'T':
			switch (myRand){
				case 0:
					blks[0]->setCoords(0,3);
					blks[1]->setCoords(1,3);
					blks[2]->setCoords(2,3);
					blks[3]->setCoords(1,4);
					t->setY(4);
					t->setOrientation(0);
					break;
				case 1:
					blks[0]->setCoords(0,3);
					blks[1]->setCoords(0,4);
					blks[2]->setCoords(0,5);
					blks[3]->setCoords(1,4);
					t->setY(5);
					t->setOrientation(3);
					break;
				case 2:
					blks[0]->setCoords(1,3);
					blks[1]->setCoords(0,4);
					blks[2]->setCoords(1,4);
					blks[3]->setCoords(2,4);
					t->setY(4);
					t->setOrientation(2);
					break;
				case 3:
					blks[0]->setCoords(0,4);
					blks[1]->setCoords(1,3);
					blks[2]->setCoords(1,4);
					blks[3]->setCoords(1,5);
					t->setY(5);
					t->setOrientation(1);
					break;
			}
			break;
		case 'J':
			switch (myRand){
				case 0:
					blks[0]->setCoords(1,3);
					blks[1]->setCoords(1,4);
					blks[2]->setCoords(0,5);
					blks[3]->setCoords(1,5);
					t->setY(5);
					t->setOrientation(3);
					break;
				case 1:
					blks[0]->setCoords(0,3);
					blks[1]->setCoords(1,3);
					blks[2]->setCoords(2,3);
					blks[3]->setCoords(2,4);
					t->setY(4);
					t->setOrientation(2);
					break;
				case 2:
					blks[0]->setCoords(0,3);
					blks[1]->setCoords(1,3);
					blks[2]->setCoords(0,4);
					blks[3]->setCoords(0,5);
					t->setY(5);
					t->setOrientation(1);
					break;
				case 3:
					blks[0]->setCoords(0,3);
					blks[1]->setCoords(0,4);
					blks[2]->setCoords(1,4);
					blks[3]->setCoords(2,4);
					t->setY(4);
					t->setOrientation(0);
					break;
			}
			break;
		case 'L':
			switch (myRand){
				case 0:
					blks[0]->setCoords(0,3);
					blks[1]->setCoords(0,4);
					blks[2]->setCoords(0,5);
					blks[3]->setCoords(1,5);
					t->setY(5);
					t->setOrientation(1);
					break;
				case 1:
					blks[0]->setCoords(2,3);
					blks[1]->setCoords(0,4);
					blks[2]->setCoords(1,4);
					blks[3]->setCoords(2,4);
					t->setY(4);
					t->setOrientation(0);
					break;
				case 2:
					blks[0]->setCoords(0,3);
					blks[1]->setCoords(1,3);
					blks[2]->setCoords(1,4);
					blks[3]->setCoords(1,5);
					t->setY(5);
					t->setOrientation(3);
					break;
				case 3:
					blks[0]->setCoords(0,3);
					blks[1]->setCoords(1,3);
					blks[2]->setCoords(2,3);
					blks[3]->setCoords(0,4);
					t->setY(4);
					t->setOrientation(2);
					break;
			}
			break;
		case 'S':
			switch (myRand){
				case 0: case 1:
					blks[0]->setCoords(1,3);
					blks[1]->setCoords(2,3);
					blks[2]->setCoords(0,4);
					blks[3]->setCoords(1,4);
					t->setY(4);
					t->setOrientation(0);
					break;
				case 2: case 3:
					blks[0]->setCoords(0,3);
					blks[1]->setCoords(0,4);
					blks[2]->setCoords(1,4);
					blks[3]->setCoords(1,5);
					t->setY(5);
					t->setOrientation(1);
					break;
			}
			break;
		case 'Z':
			switch (myRand){
				case 0: case 1:
					blks[0]->setCoords(0,3);
					blks[1]->setCoords(1,3);
					blks[2]->setCoords(1,4);
					blks[3]->setCoords(2,4);
					t->setY(4);
					t->setOrientation(0);
					break;
				case 2: case 3:
					blks[0]->setCoords(1,3);
					blks[1]->setCoords(0,4);
					blks[2]->setCoords(1,4);
					blks[3]->setCoords(0,5);
					t->setY(5);
					t->setOrientation(1);
					break;
			}
			break;
	}
	for (int i=0; i<blocksPerTetro; ++i){
		blks[i]->setBlockType(nextTetro);
		t->setTetroBlock(i, blks[i]);
		if (!isFree(blks[i]->getX(), blks[i]->getY())){
			endGame();
		}
		boardBlocks[blks[i]->getX()][blks[i]->getY()] = blks[i];
	}
	currentTetro = t;
	generateNextTetro();
}

//Purpose:	consumes void and produces void; generates the Board's next Tetro
//Pre:		True
//Post:		produces void; generates the Board's next Tetro
void Board::generateNextTetro(){
	if (level == 0){
		if (nextTetroi < maxTetros){
			nextTetro = listTetro[nextTetroi++];
		}
	}
	else {
		PRNG rng;
		if (bseed == 0){
			rng.seed(time(NULL)+1);
		}
		else {
			rng.seed(bseed);
		}
		int randTetro = rng(TetroProbs[Tetros-1]);
		if (randTetro < TetroProbs[0]){
			nextTetro = 'I';
		}
		else if (randTetro < TetroProbs[1]){
			nextTetro = 'J';
		}
		else if (randTetro < TetroProbs[2]){
			nextTetro = 'L';
		}
		else if (randTetro < TetroProbs[3]){
			nextTetro = 'O';
		}
		else if (randTetro < TetroProbs[4]){
			nextTetro = 'S';
		}
		else if (randTetro < TetroProbs[5]){
			nextTetro = 'T';
		}
		else if (randTetro < TetroProbs[6]){
			nextTetro = 'Z';
		}
	}
}

//Purpose:	Check if all of the blocks in the given Tetro are NULL and if they are
//		  	calls the Tetro's destructor
//Pre:		True
//Post:		If all the IndBlocks in the given Tetro are NULL, sets and calls the
//			Tetro's destructor
void Board::checkIndBlocks(Tetro *t) {
	bool allNULL = true;
	for (int i=0; i<blocksPerTetro; ++i){
		
		if(t->getTetroBlock(i) != NULL) {
			allNULL = false;
			break;
		}
	}
	if (allNULL && !gameOver) {
		scoreTetro(t);
	}
}

//Purpose:	Consumes void and produces void; ends the game and prints the text Game
//			Over on top of the Board
//Pre:		True	
//Post:		Produces void; ends the game and prints the text Game Over on top of the Board
void Board::endGame(){
	gameOver = true;
}

//Purpose:	Consumes two ints x and y, a char ind, and an Xwindow win and produces void;
//			draws a representative block to the Xwindow win
//Pre:		display
//Post:		Produces void; draws a representative block to the Xwindow win
void Board::drawBlock(int x, int y, char ind, Xwindow *win){
	int colour;
	switch (ind){
		case 'I': colour = win->Blue; break;
		case 'O': colour = win->Yellow; break;
		case 'T': colour = win->Orange; break;
		case 'J': colour = win->Green; break;
		case 'L': colour = win->Cyan; break;
		case 'S': colour = win->Red; break;
		case 'Z': colour = win->Magenta; break;
	}
	win->fillRectangle(x, y, 32, 32, win->Black);
	win->fillRectangle(x+1, y+1, 30, 30, colour);
}

//Purpose:	Consumes an ostream and a Board and produces an ostream representing a
//			visual interpretation of the given Board in the ostream (in text form)
//Pre:		True
//Post:		Produces an ostream representing a visual (text-based) interpretation
//			of the given Board in the ostream
std::ostream& operator<<(std::ostream &out, Board &board){
	//print display
	std::stringstream strlevel;
	std::stringstream strscore;
	std::stringstream strhiscore;
	strlevel << "Level:" << std::setw(5) << board.level;
	strscore << "Score:" << std::setw(5) << board.score;
	strhiscore << "Hi Score: " << board.highscore;
	if (board.graphics){
		board.secwin->fillRectangle(0,0,200,164,board.secwin->White);
		board.secwin->drawString(5,15,strlevel.str());
		board.secwin->drawString(5,30,strscore.str());
		board.secwin->drawString(5,45,strhiscore.str());
		if (!board.getGameOver()){
			board.secwin->drawString(5,80,"Next Block:");
			switch (board.nextTetro){
				case 'I':
					for (int i=0; i<4; ++i){
						board.drawBlock(5+31*i,90,'I',board.secwin);
					}
					break;
				case 'O':
					for (int i=0; i<2; ++i){
						for (int j=0; j<2; ++j){
							board.drawBlock(5+31*j,90+31*i,'O',board.secwin);
						}
					}
					break;
				case 'T':
					for (int i=0; i<3; ++i){
						board.drawBlock(5+31*i,90,'T',board.secwin);
					}
					board.drawBlock(36,121,'T',board.secwin);
					break;
				case 'J':
					for (int i=0; i<3; ++i){
						board.drawBlock(5+31*i,90,'J',board.secwin);
					}
					board.drawBlock(5,121,'J',board.secwin);
					break;
				case 'L':
					board.drawBlock(67,90,'L',board.secwin);
					for (int i=0; i<3; ++i){
						board.drawBlock(5+31*i,121,'L',board.secwin);
					}
					break;
				case 'S':
					for (int i=0; i<2; ++i){
						board.drawBlock(36+31*i,90,'S',board.secwin);
						board.drawBlock(5+31*i,121,'S',board.secwin);
					}
					break;
				case 'Z':
					for (int i=0; i<2; ++i){
						board.drawBlock(5+31*i,90,'Z',board.secwin);
						board.drawBlock(36+31*i,121,'Z',board.secwin);
					}					
					break;
			}
		}
		board.mainwin->fillRectangle(0, 0, 320, 569, board.mainwin->Black);
		board.mainwin->fillRectangle(5, 5, 310, 559, board.mainwin->White);
		for (int i=0; i<board.rows; ++i){
			for (int j=0; j<board.cols; ++j){
				if (board.boardBlocks[j][i] != NULL){
					board.drawBlock(5+j*31, 5+i*31, board.boardBlocks[j][i]->getBlockType(), board.mainwin);
				}
			}
		}
		if (board.getGameOver()){
			board.mainwin->fillRectangle(10, 240, 300, 45, board.mainwin->Black);
			board.mainwin->drawString(130, 255, "GAME OVER", board.mainwin->Red);
			board.mainwin->drawString(37, 275, "Type the command 'restart' to play again.", board.mainwin->Red);
		}
	}
	//print game information (level, score, etc.)
	out << strlevel.str() << std::endl;
	out << strscore.str() << std::endl;
	out << strhiscore.str() << std::endl;
	for (int i=0; i<board.cols; ++i){
		out << "-";
	}
	out << std::endl;
	//print board if game is not over, otherwise print message
	if (board.getGameOver()){
		for (int i=0; i<board.gameOverMessage; ++i){
			out << std::endl;
		}
		out << "GAME OVER" << std::endl;
		out << "Type the command 'restart' to play again." << std::endl;
		for (int i=0; i<board.rows-board.gameOverMessage; ++i){
			out << std::endl;
		}
	}
	else {
		for (int i=0; i<board.rows; ++i){
			for (int j=0; j<board.cols; ++j){
				if (board.boardBlocks[j][i] == NULL){
					out << " ";
				}
				else {
					out << board.boardBlocks[j][i]->getBlockType();
				}
			}
			out << std::endl;
		}
	}
	//print next block
	for (int i=0; i<board.cols; ++i){
		out << "-";
	}
	out << std::endl;
	if (!board.getGameOver()){
		out << "Next:" << std::endl;
		switch (board.nextTetro){
			case 'I': out << "IIII" << std::endl; break;
			case 'O': out << "OO\nOO" << std::endl; break;
			case 'T': out << "TTT\n T" << std::endl; break;
			case 'J': out << "JJJ\nJ" << std::endl; break;
			case 'L': out << "  L\nLLL" << std::endl; break;
			case 'S': out << " SS\nSS" << std::endl; break;
			case 'Z': out << "ZZ\n ZZ" << std::endl; break;
		}
	}
	return out;
}
