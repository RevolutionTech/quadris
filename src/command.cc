#include <string>
#include <iostream>
#include "command.h"

CmdNode::CmdNode() : command(""){
	for (int i=0; i<alphabet; ++i){
		letters[i] = NULL;
	}
} //constructor

CmdNode::~CmdNode(){
	command = "";
	for (int i=0; i<alphabet; ++i){
		delete letters[i];
		letters[i] = NULL;
	}
} //destructor

//Purpose:	consumes a char and produces an int that corresponds
//			to that char in accordance with the letters[] array
//			of the CmdNode struct
//Pre:		(char >= 'A' && char <= 'Z') || (char >= 'a' && char <= 'z')
//Post:		the corresponding integer is returned
int CmdNode::toSpot(char letter){
	if (letter >= 'A' && letter <= 'Z'){
		return letter - 'A';
	}
	else {
		return letter - 'a' + 26;
	}
}

//Purpose:	the reverse operation of toSpot(char); consumes an int
//			and produces a char that corresponds to that int in
//			accordance with the letters[] array of the CmdNode struct
//Pre:		int >= 0 && int <alphabet
//Post:		the corresponding char is returned
char CmdNode::fromSpot(int spot){
	if (spot < 26){
		return spot + 'A';
	}
	else {
		return spot + 'a' - 26;
	}
}

//Purpose:	consumes void and produces void; initializes CmdNodes to the default
//			configuration for the Board
//Pre:		True
//Post:		produces void; initalizes CmdNodes to the default configuration for the Board
void CmdNode::init(){
	insert("left", "left");
	insert("right", "right");
	insert("down", "down");
	insert("clockwise", "clock");
	insert("counterclockwise", "cclock");
	insert("drop", "drop");
	insert("levelup", "lvlu");
	insert("leveldown", "lvld");
	insert("restart", "restart");
	insert("rename", "rename");
}

//Purpose:	consumes two strings keyword and command and adds the given command
//			to the given keyword in the Trie (produces void)
//Pre:		command is one of the known commands
//Post:		produces void; adds the given command to the given keyword
void CmdNode::insert(const std::string &keyword, const std::string &command){
	if (keyword == ""){
		this->command = command;
	}
	else {
		if (this->letters[toSpot(keyword[0])] == NULL){
			CmdNode *c = new CmdNode;
			this->letters[toSpot(keyword[0])] = c;
		}
		this->letters[toSpot(keyword[0])]->insert(keyword.substr(1), command);
	}
}

//Purpose:	consumes a string keyword and produces void; removes the command from
//			the given keyword in the Trie
//Pre:		True
//Post:		produces void; removes the command from the given keyword
void CmdNode::remove(const std::string &keyword){
	if (keyword == ""){
		this->command = "";
	}
	else if (this->letters[toSpot(keyword[0])] != NULL){
		this->letters[toSpot(keyword[0])]->remove(keyword.substr(1));
	}
}

//Purpose:	consumes void and returns an integer that represents the amount of commands
//			that satisfy the keyword given from the current CmdNode (the amount of ambiguous
//			commands from the given keyword)
//Pre:		True
//Post:		Returns an integer that represents the amount of commands that satisfy
//			the keyword given from the current CmdNode
int CmdNode::findAmount(){
	int amount = 0;
	if (this->command != ""){
		++amount;
	}
	for (int i=0; i<alphabet; ++i){
		if (this->letters[i] != NULL){
			amount += this->letters[i]->findAmount();
		}
	}
	return amount;
}

//Purpose:	consumes a keyword and produces a string that represents the command that the
//			keyword represents
//Pre:		findAmount() == 1
//Post:		produces a string that represents the command that the keyword represents
std::string CmdNode::findCommand(const std::string &keyword){
	if (keyword != ""){
		if (this->letters[toSpot(keyword[0])] != NULL){
			return this->letters[toSpot(keyword[0])]->findCommand(keyword.substr(1));
		}
		else {
			return "";
		}
	}
	else if (this->command != ""){
		return this->command;
	}
	else {
		if (findAmount() == 1){
			for (int i=0; i<alphabet; ++i){
				if (this->letters[i] != NULL && this->letters[i]->findAmount() == 1){
					return this->letters[i]->findCommand("");
				}
			}
		}
		return "";
	}
}

//Purpose:	consumes two strings current and newcmd, and produces void; moves the
//			command from the keyword current to the keyword newcmd
//Pre:		True
//Post:		produces void; moves the command from the keyword current to the keyword
//			newcmd
void CmdNode::rename(const std::string &current, const std::string &newcmd){
	insert(newcmd, findCommand(current));
	remove(current);
}

//Purpose:	consumes a pointer to the Board and produces void; interprets command
//			as given by stdin
//Pre:		True
//Post:		produces void; interprets command as given by stdin
void CmdNode::interpretCommand(Board *board){
	//gather command from stdin
	char getCommand[256];
	std::cin.getline(getCommand, 256);
	//interpret number from start of command
	int i = 0;
	int num = 0;
	if (getCommand[i] >= '0' && getCommand[i] <= '9'){
		while (getCommand[i] >= '0' && getCommand[i] <= '9'){
			num *= 10;
			num += int(getCommand[i]) - '0';
			++i;
		}
	}
	else {
		num = 1;
	}
	//interpret actual command name
	std::string wordCommand;
	while ((getCommand[i] >= 'A' && getCommand[i] <= 'Z') || (getCommand[i] >= 'a' && getCommand[i] <= 'z')){
		wordCommand += getCommand[i];
		++i;
	}
	std::string command;
	command = findCommand(wordCommand);
	//interpret arguments
	const int numOfArgs = 2;
	std::string arg[numOfArgs];
	//interpret arguments for rename
	if (command == "rename"){
		for (int j=0; j<numOfArgs; ++j){
			++i;
			while ((getCommand[i] >= 'A' && getCommand[i] <= 'Z') || (getCommand[i] >= 'a' && getCommand[i] <= 'z')){
				arg[j] += getCommand[i];
				++i;
			}
		}
	}
	for (int i=0; i<num; ++i){
		//interpret command
		if (command == "left" && !board->getGameOver()){
			board->getCurrentTetro()->lShift();
		}
		else if (command == "right" && !board->getGameOver()){
			board->getCurrentTetro()->rShift();
		}
		else if (command == "down" && !board->getGameOver()){
			board->getCurrentTetro()->dShift();
		}
		else if (command == "clock" && !board->getGameOver()){
			board->getCurrentTetro()->rClockwise();
		}
		else if (command == "cclock" && !board->getGameOver()){
			board->getCurrentTetro()->rCClockwise();
		}
		else if (command == "drop" && !board->getGameOver()){
			board->getCurrentTetro()->drop();
		}
		else if (command == "lvlu"){
			board->levelUp();
		}
		else if (command == "lvld"){
			board->levelDown();
		}
		else if (command == "restart"){
			board->restart();
		}
		else if (command == "rename"){
			rename(arg[0], arg[1]);
		}
	}
	//print board
	if (!std::cin.eof()){
		std::cout << *board << std::endl;
	}
}
