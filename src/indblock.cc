#include "board.h"

using namespace std;

IndBlock::IndBlock() { }; //constructor

IndBlock::~IndBlock(){
	for (int i=0; i<blocksPerTetro; ++i){
		if (myTetro->getTetroBlock(i) != NULL){
			int xVal = myTetro->getTetroBlock(i)->getX();
			int yVal = myTetro->getTetroBlock(i)->getY();
			if(xVal == x && yVal == y) {
				myTetro->setTetroBlock(i, NULL);
				myTetro->getBoard()->checkIndBlocks(myTetro);
				break;
			}
		}
	}
} //destructor

//Purpose:	Consumes void and produces a pointer to the IndBlock's Tetro
//Pre:		True
//Post:		Produces a pointer to the IndBlock's Tetro
Tetro *IndBlock::getMyTetro(){
	return myTetro;
}

//Purpose:	Consumes void and produces the int that corresponds to the
//			IndBlock's x-location (getter function)
//Pre:		True
//Post:		Returns x
int IndBlock::getX(){ 
	return x;
}

//Purpose:	Consumes void and produces the int that corresponds to the
//			IndBlock's y-location (getter function)
//Pre:		True
//Post:		Returns y
int IndBlock::getY(){
	return y;
}

//Purpose:	Consumes void and produces the char that corresponds to the
//			IndBlock's blockType (getter function)
//Pre:		True
//Post:		Returns blockType
char IndBlock::getBlockType(){
	return blockType;
}

//Purpose:	Consumes a Tetro pointer and produces void; sets the IndBlock's
//			Tetro (with a pointer)
//Pre:		True
//Post:		Produces void; sets teh IndBlock's Tetro pointer
void IndBlock::setMyTetro(Tetro *t){
	this->myTetro = t;
}

//Purpose:	Consumes a char and returns void; sets the IndBlock's blockType
//Pre:		True
//Post:		Produces void; sets the IndBlock's blockType
void IndBlock::setBlockType(char type){
	this->blockType = type;
}

//Purpose:	Consumes two ints and returns void; sets the IndBlock's x-value
//			and y-value
//Pre:		True
//Post:		Produces void; sets the IndBlock's x-value and y-value
void IndBlock::setCoords(int x, int y){
	this->x = x;
	this->y = y;
}
