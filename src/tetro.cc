#include <string>
#include "board.h"

//Default constructor
Tetro::Tetro() {
	blockType = ' ';
	x = 0;
	y = 0;
	orientation = 0;
	blockLevel = -1;
}

//Tetro::~Tetro() { } //destructor

//Purpose:	Consumes void and produces a pointer to the Board
//Pre:		True
//Post:		Produces a pointer to the Board
Board *Tetro::getBoard(){
	return board;
}

//Purpose:	Consumes an int i and produces a pointer to the Ind Block from the Tetro
//Pre:		True
//Post:		Produces a pointer to the Ind Block from the Tetro
IndBlock *Tetro::getTetroBlock(int i){
	return tetroBlocks[i];
}

//Purpose: Consumes void and produces a char representing the Tetro type which
//		   is on of {O, I, J, L, Z, S, T}
//Pre: True
//Post: Returns blockType
char Tetro::getBlockType() {
	return blockType;
}

//Purpose: Consumes void and produces a int representing the x-value of location
//         of the lower left corner of the Tetro
//Pre: True
//Post: Returns x
int Tetro::getX() {
	return x;
}

//Purpose: Consumes void and produces a int representing the y-value of location
//         of the lower left corner of the Tetro
//Pre: True
//Post: Returns y
int Tetro::getY() {
	return y;
}

//Purpose: Consumes an arrray of 4 inegers and returns the element number 
// 		   which contains the largest value
//Pre:     True
//Post: returns the element number of the largest value
int Tetro::getLargest(int num[4]) {
	int element = -1;
	int largest = -1;
	for(int i = 0; i < 4; i++) {
		if(num[i] > largest) {
			largest = num[i];
			element = i;
		}
	}
	return element;
}

//Purpose: Consumes an arrray of 4 inegers and returns the element number 
// 		   which contains the smallest value
//Pre:     True
//Post: returns the element number of the smallest value
int Tetro::getSmallest(int num[4]) {
	int element = 5;
	int smallest = 20;
	for(int i = 0; i < 4; i++) {
		if(num[i] < smallest) {
			smallest = num[i];
			element = i;
		}
	}
	return element;
}

//Purpose: Consumes void and produces a int representing the level the Tetro was
//		   created on
//Pre: True
//Post: Returns blockLevel
int Tetro::getBlockLevel() {
	return blockLevel;
}

//Purpose:	Consumes a pointer to the Board and returns void; sets the Tetro's board
//			to the given Board
//Pre:		True
//Post:		Produces void; sets the Tetro's board to the given Board
void Tetro::setBoard(Board *b){
	this->board = b;
}

//Purpose:	Consumes an int i and a pointer to an Ind Block and sets the Tetro's ith
//			Ind Block to the given pointer
//Pre:		i < blocksPerTetro
//Post:		Produces void; sets the Tetro's ith Ind Block to the given pointer
void Tetro::setTetroBlock(int i, IndBlock *block){
	tetroBlocks[i] = block;
}

//Purpose: Consumes a char, type and sets the value of blockType to type
//Pre: True
//Post: void, side effect, sets blockType to type
void Tetro::setBlockType(char type) {
	blockType = type;
}

//Purpose: Consumes a int, xVal and sets the value of x to xVal
//Pre: True
//Post: void, side effect, sets x to xVal
void Tetro::setX(int xVal) {
	x = xVal;	
}

//Purpose: Consumes a int, yVal and sets the value of y to yVal
//Pre: True
//Post: void, side effect, sets y to yVal
void Tetro::setY(int yVal) {
	y = yVal;
}

//Purpose: Consumes a int, bLevel and sets the blockLevel of x to bLevel
//Pre: True
//Post: void, side effect, sets blockLevel to bLevel
void Tetro::setBlockLevel(int bLevel) {
	blockLevel = bLevel;
}

//TODO
//FIX comments
//Purpose: Consumes void and call the appropriate roation function with the
// CW (clockwise value)
//Pre: True
//Post: returns true if the change was successful and false if it was not
bool Tetro::rClockwise() {
	//Question: can a block rotate if there are block in the path of rotation 
	//			but not the destination
	switch(blockType) {
		case 'O' : return true;
		case 'I' : return iRotate("CW");
		case 'S' : return sRotate("CW");
		case 'Z' : return zRotate("CW");
		case 'L' : return lRotate("CW");
		case 'J' : return jRotate("CW");
		case 'T' : return tRotate("CW");
	}
	return false;
}

//Purpose: Consumes void and call the appropriate roation function with the
// CWW (counter-clockwise value)
//Pre: True
//Post: returns true if the change was successful and false if it was not
bool Tetro::rCClockwise() {
	//Question: can a block rotate if there are block in the path of rotation 
	//			but not the destination
	switch(blockType) {
		case 'O' : return true;
		case 'I' : return iRotate("CCW");
		case 'S' : return sRotate("CCW");
		case 'Z' : return zRotate("CCW");
		case 'L' : return lRotate("CCW");
		case 'J' : return jRotate("CCW");
		case 'T' : return tRotate("CCW");
	}
	return false;
}

//Purpose: Consumes a string dir representing the direction of rotation
//		   and returns false if the rotation was not possible and rotates the
//		   I-Block and returns true otherwise
//Pre: blockType == 'I', orientation != -1
//Post: changes the contents of board and returns true if the rotation was
//      possible and false otherwise
//Fixed
bool Tetro::iRotate(std::string dir) {
	if(orientation == 0) {
		for(int i = 1; i < 4; i++) {
			if(!board->isFree(x, y-i)) {
				return false;
			}
		}
		for(int i = 1; i < 4; i++) {
			board->moveIndBlock(x+i,y, x, y-i);
		}
		orientation = 1;
		}
	else {
		for(int i = 1; i < 4; i++) {
			if(!board->isFree(x+i,y)) {
				return false;
			}
		}	
		for(int i = 1; i < 4; i++) {
			board->moveIndBlock(x, y-i, x+i, y);
		}
		orientation = 0;
	}
	return true;
}

//Purpose: Consumes a string dir representing the direction of rotation
//		   and returns false if the rotation was not possible and rotates the
//		   S-Block and returns true otherwise
//Pre: blockType == 'S', orientation != -1
//Post: changes the contents of board and returns true if the rotation was
//      possible and false otherwise
//Fixed
bool Tetro::sRotate(std::string dir) {
	if(orientation == 0) {
		if(!board->isFree(x, y-1)) {
			return false;
		}
		if(!board->isFree(x, y-2)) {
			return false;
		}
		board->moveIndBlock(x, y, x, y-2);
		board->moveIndBlock(x+1, y, x, y-1);
		board->moveIndBlock(x+2, y-1, x+1, y);
		orientation = 1;
	}
	else {
		if(!board->isFree(x, y)) {
			return false;
		}
		if(!board->isFree(x+2, y-1)) {
			return false;
		}
		board->moveIndBlock(x, y-2, x, y);
		board->moveIndBlock(x+1, y, x+2, y-1);
		board->moveIndBlock(x, y-1, x+1, y);
		orientation = 0;
	}
	return true;
}

//Purpose: Consumes a string dir representing the direction of rotation
//		   and returns false if the rotation was not possible and rotates the
//		   Z-Block and returns true otherwise
//Pre: blockType == 'Z', orientation != -1
//Post: changes the contents of board and returns true if the rotation was
//      possible and false otherwise
//Fixed
bool Tetro::zRotate(std::string dir) {
	if(orientation == 0) {
		if(!board->isFree(x, y)) {
			return false;
		}
		if(!board->isFree(x+1, y-2)) {
			return false;
		}
		board->moveIndBlock(x+1, y, x, y);
		board->moveIndBlock(x+2, y, x+1, y-2);
		orientation = 1;
	}
	else {
		if(!board->isFree(x+1, y)) {
			return false;
		}
		if(!board->isFree(x+2, y)) {
			return false;
		}
		board->moveIndBlock(x, y, x+1, y);
		board->moveIndBlock(x+1, y-2, x+2, y);
		orientation = 0;
	}
	return true;
}

//Purpose: Consumes a string dir representing the direction of rotation
//		   and returns false if the rotation was not possible and rotates the
//		   J-Block and returns true otherwise
//Pre: blockType == 'J', orientation != -1
//Post: changes the contents of board and returns true if the rotation was
//      possible and false otherwise
//Fixed
bool Tetro::jRotate(std::string dir) {
	if(dir == "CW") {
		if(orientation == 0) {
			if(!board->isFree(x, y-2)) {
			return false;
			}
			if(!board->isFree(x+1, y-2)) {
				return false;
			}
			board->moveIndBlock(x+1, y, x, y-2);
			board->moveIndBlock(x+2, y, x+1, y-2);
			orientation = 1;
		}
		else if(orientation == 1) {
			if(!board->isFree(x+1, y-1)) {
			return false;
			}
			if(!board->isFree(x+2, y-1)) {
				return false;
			}
			if(!board->isFree(x+2, y)) {
				return false;
			}
			board->moveIndBlock(x, y, x+2, y);
			board->moveIndBlock(x, y-2, x+1, y-1);
			board->moveIndBlock(x+1, y-2, x+2, y-1);
			orientation = 2;
		}
		else if(orientation == 2) {
			if(!board->isFree(x, y)) {
				return false;
			}
			if(!board->isFree(x+1, y)) {
				return false;
			}
			if(!board->isFree(x+1, y-2)) {
				return false;
			}
			board->moveIndBlock(x, y-1, x, y);
			board->moveIndBlock(x+2, y, x+1, y);
			board->moveIndBlock(x+2, y-1, x+1, y-2);
			orientation = 3;
		}
		else {
			if(!board->isFree(x, y-1)) {
			return false;
			}
			if(!board->isFree(x+2, y)) {
				return false;
			}
			board->moveIndBlock(x+1, y-1, x, y-1);
			board->moveIndBlock(x+1, y-2, x+2, y);
			orientation = 0;
		}
	}
	else if(dir == "CCW") {
		if(orientation == 0) {
			if(!board->isFree(x+1, y-1)) {
			return false;
			}
			if(!board->isFree(x+1, y-2)) {
				return false;
			}
			board->moveIndBlock(x, y-1, x+1, y-1);
			board->moveIndBlock(x+2, y, x+1, y-2);
			orientation = 3;
		}
		else if(orientation == 1) {
			if(!board->isFree(x+1, y)) {
				return false;
			}
			if(!board->isFree(x+2, y)) {
				return false;
			}
			board->moveIndBlock(x, y-2, x+1, y);
			board->moveIndBlock(x+1, y-2, x+2, y);
			orientation = 0;
		}
		else if(orientation == 2) {
			if(!board->isFree(x, y)) {
				return false;
			}
			if(!board->isFree(x, y-2)) {
				return false;
			}
			if(!board->isFree(x+1, y-2)) {
				return false;
			}
			board->moveIndBlock(x+1, y-1, x, y);
			board->moveIndBlock(x+2, y-1, x, y-2);
			board->moveIndBlock(x+2, y, x+1, y-2);
			orientation = 1;
		}
		else {
			if(!board->isFree(x, y-1)) {
			return false;
			}
			if(!board->isFree(x+2, y)) {
				return false;
			}
			if(!board->isFree(x+2, y-1)) {
				return false;
			}
			board->moveIndBlock(x, y, x+2, y);
			board->moveIndBlock(x+1, y, x+2, y-1);
			board->moveIndBlock(x+1, y-2, x, y-1);
			orientation = 2;
		}
	}	
	return true;
}

//Purpose: Consumes a string dir representing the direction of rotation
//		   and returns false if the rotation was not possible and rotates the
//		   L-Block and returns true otherwise
//Pre: blockType == 'L', orientation != -1
//Post: changes the contents of board and returns true if the rotation was
//      possible and false otherwise
//Fixed
bool Tetro::lRotate(std::string dir) {
	if(dir == "CW") {
		if(orientation == 0) {
			if(!board->isFree(x, y-2)) {
			return false;
			}
			if(!board->isFree(x, y-1)) {
				return false;
			}
			board->moveIndBlock(x+2, y, x, y-2);
			board->moveIndBlock(x+2, y-1, x, y-1);
			orientation = 1;
		}
		else if(orientation == 1) {
			if(!board->isFree(x+1, y-1)) {
			return false;
			}
			if(!board->isFree(x+2, y-1)) {
				return false;
			}
			board->moveIndBlock(x, y-2, x+1, y-1);
			board->moveIndBlock(x+1, y, x+2, y-1);
			orientation = 2;
		}
		else if(orientation == 2) {
			if(!board->isFree(x, y-2)) {
				return false;
			}
			if(!board->isFree(x+1, y-2)) {
				return false;
			}
			if(!board->isFree(x+1, y)) {
				return false;
			}
			board->moveIndBlock(x, y-1, x, y-2);
			board->moveIndBlock(x, y, x+1, y-2);
			board->moveIndBlock(x+2, y-1, x+1, y);
			orientation = 3;
		}
		else {
			if(!board->isFree(x, y)) {
			return false;
			}
			if(!board->isFree(x+2, y)) {
				return false;
			}
			if(!board->isFree(x+2, y-1)) {
				return false;
			}
			board->moveIndBlock(x, y-2, x, y);
			board->moveIndBlock(x+1, y-2, x+2, y);
			board->moveIndBlock(x+1, y-1, x+2, y-1);
			orientation = 0;
		}
	}
	else if(dir == "CCW") {
		if(orientation == 0) {
			if(!board->isFree(x+1, y-2)) {
			return false;
			}
			if(!board->isFree(x, y-2)) {
				return false;
			}
			if(!board->isFree(x+1, y-1)) {
				return false;
			}
			board->moveIndBlock(x, y, x+1, y-1);
			board->moveIndBlock(x+2, y, x+1, y-2);
			board->moveIndBlock(x+2, y-1, x, y-2);
			orientation = 3;
		}
		else if(orientation == 1) {
			if(!board->isFree(x+2, y-1)) {
				return false;
			}
			if(!board->isFree(x+2, y)) {
				return false;
			}
			board->moveIndBlock(x, y-1, x+2, y-1);
			board->moveIndBlock(x, y-2, x+2, y);
			orientation = 0;
		}
		else if(orientation == 2) {
			if(!board->isFree(x+1, y)) {
				return false;
			}
			if(!board->isFree(x, y-2)) {
				return false;
			}
			board->moveIndBlock(x+1, y-1, x+1, y);
			board->moveIndBlock(x+2, y-1, x, y-2);
			orientation = 1;
		}
		else {
			if(!board->isFree(x, y-1)) {
				return false;
			}
			else if(!board->isFree(x, y)) {
				return false;
			}
			else if(!board->isFree(x+2, y-1)) {
				return false;
			}
			board->moveIndBlock(x, y-2, x, y-1);
			board->moveIndBlock(x+1, y-2, x, y);
			board->moveIndBlock(x+1, y, x+2, y-1);
			orientation = 2;
		}
	}	
	return true;
}

//Purpose: Consumes a string dir representing the direction of rotation
//		   and returns false if the rotation was not possible and rotates the
//		   T-Block and returns true otherwise
//Pre: blockType == 'T', orientation != -1
//Post: changes the contents of board and returns true if the rotation was
//      possible and false otherwise
bool Tetro::tRotate(std::string dir) {
	if(dir == "CW") {
		if(orientation == 0) {
			if(!board->isFree(x+1, y-2)) {
				return false;
			}
			board->moveIndBlock(x+2, y-1, x+1, y-2);
			orientation = 1;
		}
		else if(orientation == 1) {
			if(!board->isFree(x, y)) {
				return false;
			}
			if(!board->isFree(x+2, y)) {
				return false;
			}
			board->moveIndBlock(x+1, y-2, x, y);
			board->moveIndBlock(x, y-1, x+2, y);
			orientation = 2;
		}
		else if(orientation == 2) {
			if(!board->isFree(x, y-1)) {
				return false;
			}
			if(!board->isFree(x, y-2)) {
				return false;
			}
			board->moveIndBlock(x+2, y, x, y-1);
			board->moveIndBlock(x+1, y, x, y-2);
			orientation = 3;
		}
		else {
			if(!board->isFree(x+1, y)) {
				return false;
			}
			if(!board->isFree(x+2, y-1)) {
				return false;
			}
			board->moveIndBlock(x, y-2, x+1, y);
			board->moveIndBlock(x, y, x+2, y-1);
			orientation = 0;
		}
	}
	else if(dir == "CCW") {
		if(orientation == 0) {
			if(!board->isFree(x, y)) {
				return false;
			}
			if(!board->isFree(x, y-2)) {
				return false;
			}
			board->moveIndBlock(x+1, y, x, y);
			board->moveIndBlock(x+2, y-1, x, y-2);
			orientation = 3;
		}
		else if(orientation == 1) {
			if(!board->isFree(x+2, y-1)) {
				return false;
			}
			board->moveIndBlock(x+1, y-2, x+2, y-1);
			orientation = 0;
		}
		else if(orientation == 2) {
			if(!board->isFree(x, y-1)) {
				return false;
			}
			if(!board->isFree(x+1, y-2)) {
				return false;
			}
			board->moveIndBlock(x, y, x, y-1);
			board->moveIndBlock(x+2, y, x+1, y-2);
			orientation = 1;
		}
		else {
			if(!board->isFree(x+2, y)) {
				return false;
			}
			if(!board->isFree(x+1, y)) {
				return false;
			}
			board->moveIndBlock(x, y-1, x+2, y);
			board->moveIndBlock(x, y-2, x+1, y);
			orientation = 2;
		}
	}
	return true;
}

//Puprose: Consumes void and moves the tetro to the lowest possible possition
//Pre: True
//Post: Changes the contents of board to reflect the moved tetro
void Tetro::drop(){ 
	int *order = new int[4];
	int i;
	bool badDrop = false;
	bool dropped = false;
	int xVal;
	int yVal;
	
	getOrder(order, "down");
	
	int dropDiff = 17 - tetroBlocks[order[0]]->getY();
	
	//Note from Lucas: I changed some of this code, I think I fixed some of the problems
	//you're having, but I'm sorry if I messed it up
	//<Lucas changed your code>
	while(dropDiff > 0 && dropped == false) {
		for(i = 0; i < 4; ++i) {
			xVal = tetroBlocks[order[i]]->getX();
			yVal = tetroBlocks[order[i]]->getY();
			if(!board->isFree(xVal, yVal+dropDiff)) {
				if(!inTetro(xVal, yVal+dropDiff)) {
					badDrop = true;
					break;
				}
			}
		}
		
		if(badDrop == true) {
			--dropDiff;
			badDrop = false;
		}
		else {
			for(i = 0; i < 4; i++) {
				xVal = tetroBlocks[order[i]]->getX();
				yVal = tetroBlocks[order[i]]->getY();
				board->moveIndBlock(xVal, yVal, xVal, yVal+dropDiff);
			}
			dropped = true;
		}
	}
	
	int *rows = new int[4];
	for(i=0; i < 4; i++) {
		rows[i] = tetroBlocks[i]->getY();
	}
	
	board->checkRows(rows);
	board->createTetro();
	
	delete [] rows;
	delete [] order;
	//</Lucas changed your code>
}

//Puprose:	Consumes void and moves the tetro to the left if possible
//Pre:		True
//Post:		Returns true and changes the of board to reflect the change if it was
//			possible and false otherwise
void Tetro::lShift(){ 
	int *order = new int[4];
	int i;
	int xVal;
	int yVal;
	bool possible = true;
	
	getOrder(order, "left");
	
	for(i = 0; i < 4; ++i) {
		xVal = tetroBlocks[order[i]]->getX();
		yVal = tetroBlocks[order[i]]->getY();
		if(xVal-1 < 0) {
			possible = false;
			break;
		}
		
		if(!board->isFree(xVal-1, yVal)) {
			if(!inTetro(xVal-1, yVal)) {
				possible = false;
				break;
			}
		}
	}

	if(possible) {
		for(i = 0; i < 4; ++i) {
			xVal = tetroBlocks[order[i]]->getX();
			yVal = tetroBlocks[order[i]]->getY();
			board->moveIndBlock(xVal, yVal, xVal-1, yVal);
		}
		this->x = x-1;
	}
	delete [] order;
}

//Puprose: Consumes void and moves the tetro to the right if possible
//Pre: True
//Post: Returns true and changes the of board to reflect the change if it was
//		possible and false otherwise
void Tetro::rShift(){
	int *order = new int[4];
	int i;
	int xVal;
	int yVal;
	bool possible = true;
	
	getOrder(order, "right");
	
	
	for(i = 0; i < 4; ++i) {
		xVal = tetroBlocks[order[i]]->getX();
		yVal = tetroBlocks[order[i]]->getY();
		
		if(xVal+1 > 9) {
			possible = false;
			break;
		}
		
		if(!board->isFree(xVal+1, yVal)) {
			if(!inTetro(xVal+1, yVal)) {
				possible = false;
				break;
			}
		}
	}
	
	if(possible) {
		for(i = 0; i < 4; ++i) {
			xVal = tetroBlocks[order[i]]->getX();
			yVal = tetroBlocks[order[i]]->getY();
			board->moveIndBlock(xVal, yVal, xVal+1, yVal);
		}
		this->x = x+1;
	}
	delete [] order;
}

//Puprose: Consumes void and moves the tetro to down if possible
//Pre: True
//Post: Returns true and changes the of board to reflect the change if it was
//		possible and false otherwise
void Tetro::dShift(){ 
	int *order = new int[4];
	int i;
	int xVal;
	int yVal;
	bool possible = true;
	
	getOrder(order, "down");
	
	for(i = 0; i < 4; i++) {
		xVal = tetroBlocks[order[i]]->getX();
		yVal = tetroBlocks[order[i]]->getY();
		
		if(yVal+1 > 18) {
			possible = false;
			break;
		}
		
		if(!board->isFree(xVal, yVal+1)) {
			if(!inTetro(xVal, yVal+1)) {
				possible = false;
				break;
			}
		}
	}
	
	if(possible) {
		for(i = 0; i < 4; i++) {
			xVal = tetroBlocks[order[i]]->getX();
			yVal = tetroBlocks[order[i]]->getY();
			board->moveIndBlock(xVal, yVal, xVal, yVal+1);
		}
		this->y = y+1;
	}
	
	delete [] order;
}

//Puprose: Consumes 2 ints xVal and yVal and returns true if the position
//		   (xVal, yVal) is in the tetro and false otherwise
//Pre: True
//Post: returns true if the position (xVal, yVal) is in the tetro
//      and false otherwise
bool Tetro::inTetro(int xVal, int yVal) {
	for(int i = 0; i < 4; i++) {
		int xTetro = tetroBlocks[i]->getX();
		int yTetro = tetroBlocks[i]->getY();
		if( (xVal == xTetro) && (yVal == yTetro) ) {
			return true;
		}
	}
	return false;
}

//Puprose: Consumes a int pointer order and a string dir and changes order to
//		   represent the order needed to move tetro in dir
//Pre: True
//Post: order is sorted for movement in direction dir
void Tetro::getOrder(int *order, std::string dir) {
	int xVals[4];
	int yVals[4];
	int val;
	int *temp = new int[4];
	bool isSorted = false;
	
	for(int i = 0; i < 4; i++) {
		xVals[i] = tetroBlocks[i]->getX();
		yVals[i] = tetroBlocks[i]->getY();
	}
	
	if(dir == "left") {
		
		//fill temp with the xVals
		for(int i = 0; i < 4; i++) {
			temp[i] = xVals[i];
		}
		
		//get order sorted in terms of x
		for(int i=0; i < 4; i++) {
			val = getSmallest(temp);
			order[i] = val;
			temp[val] = 20;
		}
		
		int i = 0;
		
		while(!isSorted) {
		
			//check if list is sorted
			if(i == 3) {
				isSorted = true;
			}
			//check if consequtive xVals (in terms of order) are eqaul
			else if(yVals[order[i]] == yVals[order[i+1]]) {
				//check if consequtive yVals (in terms of order) are 
				//sorted correctly
				if(xVals[order[i]] > xVals[order[i+1]]) {
					int temp2 = order[i+1];
					order[i+1] = order[i];
					order[i] = temp2;
					i = 0;
				}
				else {
					++i;
				}
			}
			else {
				++i;
			}
		}
		
	}
	
	else if(dir == "right") {
	
		//fill temp with the xVals
		for(int i = 0; i < 4; ++i) {
			temp[i] = xVals[i];
		}
		
		//get order sorted in terms of x
		for(int i=0; i < 4; ++i) {
			val = getLargest(temp);
			order[i] = val;
			temp[val]  = -1;
		}
		
		int i = 0;
		
		while(!isSorted) {

			//check if list is sorted
			if(i == 3) {
				isSorted = true;
			}
			//check if consequtive xVals (in terms of order) are eqaul
			else if(xVals[order[i]] == xVals[order[i+1]]) {
				//check if consequtive yVals (in terms of order) are 
				//sorted correctly
				if(yVals[order[i]] < yVals[order[i+1]]) {
					int temp2 = order[i+1];
					order[i+1] = order[i];
					order[i] = temp2;
					i = 0;
				}
				else {
					i++;
				}
			}
			else {
				i++;
			}
		}
	}
	
	else if(dir == "down") {
	
		//fill temp with the yVals
		for(int i = 0; i < 4; i++) {
			temp[i] = xVals[i];
		}
		
		//get order sorted in terms of x
		for(int i=0; i < 4; i++) {
			val = getLargest(temp);
			order[i] = val;
			temp[val] = -1;
		}
		
		int i = 0;
		while(!isSorted) {
		
			//check if list is sorted
			if(i == 3) {
				isSorted = true;
			}
			//check if consequtive xVals (in terms of order) are eqaul
			else if(xVals[order[i]] == xVals[order[i+1]]) {
				//check if consequtive yVals (in terms of order) are 
				//sorted correctly
				if(yVals[order[i]] < yVals[order[i+1]]) {
					int temp2 = order[i+1];
					order[i+1] = order[i];
					order[i] = temp2;
					i = 0;
				}
				else {
					i++;
				}
			}
			else {
				i++;
			}
		}
	}
	
	delete [] temp;
}

//Purpose: Consumes void and produces a int representing the current orientation 
//		   phase of the Tetro
//Pre: True
//Post: Returns orientation
int Tetro::getOrientation() {
	return orientation;
}

//Purpose: Consumes a int, ori and sets the value of orientation to ori
//Pre: True
//Post: void, side effect, sets orientation to ori
void Tetro::setOrientation(int ori) {
	orientation = ori;
}
